﻿using System.Collections.Generic;

namespace BigDataSmartSolutions.GeradorCSV.Web.Models
{
    public class ImportacaoModel
    {
        public string MensagemErro { get; set; }

        public string MensagemSucesso { get; set; }

        public string SessionID { get; set; }

        public IList<string> Planilhas { get; set; }
    }
}