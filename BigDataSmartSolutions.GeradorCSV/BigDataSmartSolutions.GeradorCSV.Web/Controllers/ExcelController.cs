﻿using BigDataSmartSolutions.GeradorCSV.Web.Models;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BigDataSmartSolutions.GeradorCSV.Web.Controllers
{
    public class ExcelController : Controller
    {
        private IntPtr porteiro { get; set; }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Importar(HttpPostedFileBase arquivo)
        {
            ImportacaoModel model = new ImportacaoModel() { Planilhas = new List<string>() };

            if (arquivo == null || arquivo.ContentLength == 0)
            {
                model.MensagemErro = "Selecione um arquivo.";
            }
            else if (arquivo.ContentLength > 5242880)
            {
                model.MensagemErro = "Selecione um arquivo menor que 5MB.";
            }
            else if (!arquivo.FileName.Contains(".xlsx") && !arquivo.FileName.Contains(".xls"))
            {
                model.MensagemErro = "Selecione um arquivo do tipo excel.";
            }
            else
            {
                string session = HttpContext.Session.SessionID;
                string path = Server.MapPath(string.Format("~/App_Data/{0}", session));
                string extersao = (new FileInfo(arquivo.FileName)).Extension.Replace(".", string.Empty);
                string caminho_excel = string.Format("{0}/excel.{1}", path, extersao);

                // Verificar se a pasta temporaria existe na App_Data, se não, criar.
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                // Verificar se o arquivo temporario existe, se sim, deletar.
                if (System.IO.File.Exists(caminho_excel))
                    System.IO.File.Delete(caminho_excel);

                // Salva arquivo na pasta temporaria.
                arquivo.SaveAs(caminho_excel);

                Application excel = new Application();
                excel.Visible = false;
                excel.DisplayAlerts = false;
                excel.Workbooks.Open(caminho_excel);

                // Setar porteiro do processo.
                this.porteiro = new IntPtr(excel.Hwnd);

                foreach (Worksheet planilha in excel.Worksheets)
                {
                    model.Planilhas.Add(planilha.Name);
                }

                excel.Workbooks.Close();
                excel.Quit();

                // Finalizar processo do excel
                FinalizarProcesso();

                model.SessionID = session;
                model.MensagemSucesso = "Arquivo importado com sucesso.";
            }

            return View("Index", model);
        }

        [HttpGet]
        public FileResult ConverterCSV(string pasta, string planilha)
        {
            string path = Server.MapPath(string.Format("~/App_Data/{0}", pasta));
            string arquivo_excel = Directory.EnumerateFiles(path).First();
            string arquivo_csv = string.Format("{0}/{1}.csv", path, planilha);

            Application excel = new Application();
            excel.Visible = false;
            excel.DisplayAlerts = false;
            excel.Workbooks.Open(arquivo_excel);

            // Setar porteiro do processo.
            this.porteiro = new IntPtr(excel.Hwnd); 

            Worksheet sheet = excel.Worksheets[planilha];
            sheet.SaveAs(arquivo_csv, Microsoft.Office.Interop.Excel.XlFileFormat.xlCSVWindows);

            excel.Workbooks.Close();
            excel.Quit();

            // Finalizar processo do excel
            FinalizarProcesso();

            return File(arquivo_csv, "text/csv", string.Format("{0}.csv", planilha));
        }

        [HttpPost]
        public ActionResult LimparPastaTemporario(string pasta)
        {
            string path = Server.MapPath(string.Format("~/App_Data/{0}", pasta));

            // Excluir pasta temporaria
            if (Directory.Exists(path))
                Directory.Delete(path, true);

            return null;
        }

        private void FinalizarProcesso()
        {
            foreach (System.Diagnostics.Process proc in System.Diagnostics.Process.GetProcessesByName("Excel"))
            {
                if (proc.Handle == this.porteiro)
                {
                    proc.Kill();
                }
            }
        }
    }
}
